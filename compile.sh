#!/bin/bash
set -e
#--------------------------------------------------------- build command
export MAVEN_CMD="mvn -B \
                  -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn \
                  -Drevision=${PSCI_VERSION}\
                  "

$MAVEN_CMD package -Dmaven.test.skip=${MAVEN_TEST_SKIP}
